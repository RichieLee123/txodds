package collatzconjecture

import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

class CollatzConjectureStreamTests extends AnyFlatSpecLike with Matchers {
  behavior of "collatzFn"
  it should "half any even number" in {
    CollatzConjectureStream.collatzFn(2) shouldEqual 1
    CollatzConjectureStream.collatzFn(10) shouldEqual 5
    CollatzConjectureStream.collatzFn(100) shouldEqual 50
    CollatzConjectureStream.collatzFn(1000000) shouldEqual 500000
  }

  it should "triple and add 1 to any odd number" in {
    CollatzConjectureStream.collatzFn(5) shouldEqual 16
    CollatzConjectureStream.collatzFn(7) shouldEqual 22
    CollatzConjectureStream.collatzFn(111) shouldEqual 334
    CollatzConjectureStream.collatzFn(999) shouldEqual 2998
  }

  behavior of "collatzStream"
  it should "reduce down to 1 for a given value" in {
    CollatzConjectureStream.create(10).take(7).toList shouldEqual List(10,5,16,8,4,2,1)
    CollatzConjectureStream.create(15).take(18).toList shouldEqual List(15,46,23,70,35,106,53,160,80,40,20,10,5,16,8,4,2,1)
  }

  it should "repeat back to the starting value once the reduction is complete" in {
    CollatzConjectureStream.create(10).take(9).toList shouldEqual List(10,5,16,8,4,2,1,10,5)
    CollatzConjectureStream.create(15).take(20).toList shouldEqual List(15,46,23,70,35,106,53,160,80,40,20,10,5,16,8,4,2,1,15,46)
  }
}
