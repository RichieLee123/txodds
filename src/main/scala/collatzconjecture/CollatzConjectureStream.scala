package collatzconjecture

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.Source
import cats.effect.IO
import fs2.{Pure, Stream}

import scala.concurrent.duration.DurationInt

// original idea was something akin to this but i cant work out if HTTP4S supports SSE protocol exactly
object CollatzConjectureStream {
  private[collatzconjecture] val collatzFn: Int => Int = value => {
    value % 2 match {
      case 0 => value / 2
      case _ => (value * 3) + 1
    }
  }

  private[collatzconjecture] def collatzStream(start: Int): Stream[Pure, Int] = {
    val collatzStream: Int => Stream[Pure, Int] = elem =>
      Stream.iterate(elem)(_ + 1).takeThrough(_ != 1)
    Stream(start).repeat.flatMap(collatzStream)
  }

  def create(start: Int): Stream[Pure, Any] = {
    collatzStream(start).intersperse(Stream.awakeEvery[IO](1.seconds))
  }
}


object CollatzConjectureAkka {
  private[collatzconjecture] val collatzFn: Int => Int = value => {
    value % 2 match {
      case 0 => value / 2
      case _ => (value * 3) + 1
    }
  }

  def create(start: Int): Source[Int, NotUsed] = {
    implicit val actorSystem = ActorSystem("default")
    implicit val ex = actorSystem.dispatcher
    val collatzList = Stream.iterate(start)(collatzFn).takeThrough(_ != 1).toList
    Source.unfold(start)(elem => {
      if (elem == 1) None
      else Some(collatzFn(elem), elem)
    })

    Source.cycle(() => collatzList.iterator)
  }
}
