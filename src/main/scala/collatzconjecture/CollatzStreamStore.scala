package collatzconjecture

import akka.NotUsed
import akka.stream.scaladsl.Source

import scala.collection.mutable

object CollatzStreamStore {
  val streamMap = mutable.Map[Int, Source[Int, NotUsed]]()

  def add(id: Int, stream: Source[Int, NotUsed]): Option[Int] = {
    if (streamMap.contains(id)) None
    else {
      streamMap += (id -> stream)
      Some(id)
    }
  }

  def remove(id:Int): Unit = streamMap.remove(id)
  def get(id: Int): Option[Source[Int, NotUsed]] = streamMap.get(id)
  def getAll: List[Source[Int, NotUsed]] = streamMap.toList.map(_._2)
  def increment(id: Int, value: Int): Option[Int] = {
    streamMap.get(id)
      .map(str => {
        streamMap(id) = str.map(e => e + value)
        id
      })
  }
}
