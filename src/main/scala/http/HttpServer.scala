package http

import akka.actor.ActorSystem
import akka.http.scaladsl.Http

object HttpServer extends App {
  implicit val actorSystem = ActorSystem("default")
  implicit val ex = actorSystem.dispatcher

  val bindingFuture = Http().newServerAt( "localhost", 8080).bind(Routes.provide)

  // i dont know why for the life of me but the application keeps shutting down whenever its not ran in intellij.
  // this is a bodge to fix this
  while (true) {
    Thread.sleep(1000)
  }

//  Future.never.flatMap(_ => bindingFuture).flatMap(_.unbind()).onComplete(_ => actorSystem.terminate())
}
