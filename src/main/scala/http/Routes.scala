package http

import akka.http.scaladsl.model.sse.ServerSentEvent
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.scaladsl.{Merge, Source}
import collatzconjecture.{CollatzConjectureAkka, CollatzStreamStore}

import scala.concurrent.duration.DurationInt

object Routes {
  def provide: Route = topLevelRoute

  private val topLevelRoute: Route =
    concat(
      pathPrefix("create" / IntNumber)(createStreamRoute),
      pathPrefix("messages" )(messagesStreamRoute),
      path("destroy" / IntNumber)(destroyStreamRoute),
    )

  private def createStreamRoute(id: Int): Route = {
    path(IntNumber) (startingNumber => {
      concat(
        post {
          val stream = CollatzConjectureAkka.create(startingNumber)
          CollatzStreamStore.add(id, stream)
          complete(id.toString)
        }
      )
    })
  }

  private def messagesStreamRoute: Route = {
    import akka.http.scaladsl.marshalling.sse.EventStreamMarshalling._

    def getAllMessagesRoute: Route = {
      get {
        val sources = CollatzStreamStore.getAll
        val merged = sources.fold(Source.empty)((a,b) => Source.combine(a,b)(Merge(_)))
        complete(merged.delay(1.second).map(elem => ServerSentEvent(elem.toString)))
      }
    }

    def getMessagesRoute: Route = {
      path(IntNumber)(id => {
        get{
          CollatzStreamStore.get(id) match {
            case Some(stream) =>
              complete(
                stream.delay(1.second).map(elem => ServerSentEvent(elem.toString))
              )
            case None => reject()
          }
        }
      })
    }

    concat(
      getAllMessagesRoute,
      getMessagesRoute
    )
  }

  private def destroyStreamRoute(id: Int): Route = {
    concat(
      post {
        CollatzStreamStore.remove(id)
        complete("done")
      }
    )
  }
}
