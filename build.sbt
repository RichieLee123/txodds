ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

mainClass in (Compile, run) := Some("http.HttpServer")

lazy val root = (project in file("."))
  .settings(
    name := "TXOdds"
  )

val akkaHttpVersion = "10.2.9"
val akkaVersion = "2.6.19"
val fs2Version = "3.2.7"

val scalaTestVersion = "3.2.11"

resolvers += Resolver.bintrayRepo("krasserm", "maven")

val mainLibraries = Seq(
  "com.typesafe.akka" %% "akka-http"   % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.lightbend.akka" %% "akka-stream-alpakka-sse" % "3.0.4",
  "co.fs2" %% "fs2-core" % fs2Version,
)

val testLibraries = Seq(
  "org.scalatest" %% "scalatest" % scalaTestVersion % "test"
)

libraryDependencies ++= mainLibraries ++ testLibraries

enablePlugins(JavaServerAppPackaging, DockerPlugin)
dockerExposedPorts := Seq(8080)
