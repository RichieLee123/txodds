# txOdds tech test

## to run:
from a local machine: `sbt run`
in a docker image: (not 100% working. for some reason the port-exposure isn't working and im way out of time)
```
sbt docker:publishLocal
docker run txodds:0.1.0-SNAPSHOT
```

# notes

honestly found this excercise quite difficult.

my first thought's were to use FS2 and Http4s seeing how they are both based upon streaming and its almost trivial to create an infintely repeating stream in fs2.
unfortunately it looked like http4s didnt explicitly say it supported SSE which ruled out me using it.

SSE -   looking over the wikipedia entry linked - i could only see Akka being the library that implements the SSE protocol.
        potentially HTTP4s could implement it but i couldn't find any documentation to say that its supported.

akka streams -  i was having a lot of trouble finding a method of getting the streams to repeat themselves (the only context ive ever really worked with akka streams is specifically with kafka).
                i finally got them to a point where i could get them to cyclically emit but it was late in the day.

akka http -     not a library ive ever used before so i had to spend a fair amount of time learning how to implement its routing system. took a little time to figure out the teething problems there too

i attempted to start with a proper TDD approach but with all the learning and wrangling ive been doing its ended up being more like compiler-driven-development than test-driven-development
